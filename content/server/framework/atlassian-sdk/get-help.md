---
title: "Get help"
platform: server
product: atlassian-sdk
category: help
subcategory: help
date: "2017-11-30"
layout: get-help
---

If you are looking for status updates with our products or Atlassian Server App framework, no need to file a ticket. Just check out the latest notifications and status in the links below.
