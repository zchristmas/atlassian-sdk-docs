---
aliases:
- /server/framework/atlassian-sdk/table-names-5669189.html
- /server/framework/atlassian-sdk/table-names-5669189.md
category: devguide
confluence_id: 5669189
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=5669189
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=5669189
date: '2017-12-08'
guides: guides
legacy_title: Table names
platform: server
product: atlassian-sdk
subcategory: learning
title: Table names
---
# Table names

How are database tables named? 

Let's take the example of an entity with class name `MyObject`, belonging to a plugin with a key of `com.atlassian.example.ao.myplugin`.

The table name for this entity will be **AO\_28BE2D\_MY\_OBJECT**.

It has three parts:

1.  `AO` is a prefix common to all Active Objects generated tables. It allows us to prevent collisions with existing product table names.
2.  `28BE2D` is the last 6 characters of the hexadecimal value of the MD5 hash of plugin key name, `com.atlassian.example.ao.myplugin`, or if present,  the [namespace attribute on the ao module](/server/framework/atlassian-sdk/active-objects-plugin-module). This generated value helps prevent table name collisions between plugins that use Active Objects.

{{% note %}}

We recommend that you avoid the use of the plugin key for generating this value by setting a `namespace` for the module. This reduces the likelihood that a change in the plugin key (or more accurately, an internal representation of the plugin key name) causes new tables to be created. In this event, any existing tables are retained, but orphaned.

{{% /note %}}

3. `MY_OBJECT` is the upper case translation of the entity class name `MyObject`. The Active Object plugin chooses upper case to ease compatibility with multiple databases. This is not the default behavior of the Active Objects library so be wary about this when reading the Active Objects documentation.

You can also specify the table name you'd like to use through the `net.java.ao.schema.Table` annotation. The value defined in the annotation will still be processed in the same way a class name would be processed. So if you set the value as `MyObject`, the table will be named **AO\_28BE2D\_MY\_OBJECT**.

## Constraints

Table names (once transformed) can not be more than 30 characters long. This is an Oracle restriction that is take in account whatever database one works with to provide consistency across all supported databases.

## See also

-   [Column names](/server/framework/atlassian-sdk/column-names)
